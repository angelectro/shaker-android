package com.angelectro.shakerdetection.data.model;

/**
 * Created by Загит Талипов on 24.04.2017.
 */

public class SlackResponse {
    private Boolean ok;

    public Boolean getOk() {
        return ok;
    }

    public void setOk(Boolean ok) {
        this.ok = ok;
    }

}
